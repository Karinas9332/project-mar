let 
serviceCards=document.querySelectorAll('.card_cont');
categoryType=document.querySelectorAll('.category_type');
modalForProd=document.querySelector('.modal-for-prod');
modalTitle=document.querySelector('.modal-n-title');
btnClose=document.getElementById('modal-close');
overlay=document.getElementById('overlay');

modalPhoto=document.querySelectorAll('.modal_photo');
modalPhotoSec=document.querySelectorAll('.modal_photo2');
modalTxt=document.querySelectorAll('.modal_txt');

serviceCards.forEach(function (item, indexCard) {
    item.addEventListener('click', function () {
        index = indexCard;
        modalTitleValue(index);
        modalActive();
        getPhoto(index);
        showProps(index); 
})
})
function modalActive(){
    modalForProd.classList.add('active');
}
function  modalTitleValue(ind){
        modalTitle.innerHTML=categoryType[ind].innerText;
}
function Close(){
    modalForProd.classList.remove('active');
 }
btnClose.addEventListener('click',Close)
overlay.addEventListener('click',Close)


// arr_objects
arrayPhoto = [
    { 
        src1 : "img/service_1.jpg", 
        src2 : "img/service_1.jpg",
        src3 : "img/service_1.jpg",
        src4 : "img/service_1.jpg",
        src5 : "img/service_1.jpg",
        src6 : "img/service_1.jpg",
        txt1 : "Текст 1.  За 11 лет разработки сайтов клиентами компании стали более 300 российских и зарубежных компаний. Среди них – «Китгоф», «Эриант», «Вамсунг» и «Макрософт». Адаптивный дизайн, продуманная система обратной связи, впечатляющий функционали удобная админка – все, что нужно для работы!",
        txt2 :" Ваш проект будет вести отдельный менеджер, который горит работой и спит 1 час в сутки.Напишите ему в 3 ночи в Instagram, Telegram, Viber, WhatsApp, Вконтакте, и он поможети ответит на ваш вопрос. Пожалуй, нет такой отрасли бизнеса, для которой мы не делалисайты. Строительные компании, образование, торговля, инфобизнес, шоу-индустрия и туризм, производство и услуги... Просто запросите у нас портфолио!",
    },
    { 
        src1 :  "img/service_1.jpg", 
        src2  : "img/service_1.jpg",
        src3  : "img/service_1.jpg",
        src4 : "img/service_1.jpg",
        src5  : "img/service_1.jpg",
        src6  : "img/service_1.jpg",
        txt1 : "Текст 2.  За 11 лет разработки сайтов клиентами компании стали более 300 российских и зарубежных компаний. Среди них – «Китгоф», «Эриант», «Вамсунг» и «Макрософт». Адаптивный дизайн, продуманная система обратной связи, впечатляющий функционали удобная админка – все, что нужно для работы!",
        txt2 :" Ваш проект будет вести отдельный менеджер, который горит работой и спит 1 час в сутки.Напишите ему в 3 ночи в Instagram, Telegram, Viber, WhatsApp, Вконтакте, и он поможети ответит на ваш вопрос. Пожалуй, нет такой отрасли бизнеса, для которой мы не делалисайты. Строительные компании, образование, торговля, инфобизнес, шоу-индустрия и туризм, производство и услуги... Просто запросите у нас портфолио!",
     },
     { 
        src1 :  "img/service_3.jpg", 
        src2  : "img/service_3.jpg",
        src3  : "img/service_3.jpg",
        src4 : "img/service_3.jpg",
        src5  : "img/service_3.jpg",
        src6  : "img/service_3.jpg",
        txt1 : "Текст 3. За 11 лет разработки сайтов клиентами компании стали более 300 российских и зарубежных компаний. Среди них – «Китгоф», «Эриант», «Вамсунг» и «Макрософт». Адаптивный дизайн, продуманная система обратной связи, впечатляющий функционали удобная админка – все, что нужно для работы!",
        txt2 :" Ваш проект будет вести отдельный менеджер, который горит работой и спит 1 час в сутки.Напишите ему в 3 ночи в Instagram, Telegram, Viber, WhatsApp, Вконтакте, и он поможети ответит на ваш вопрос. Пожалуй, нет такой отрасли бизнеса, для которой мы не делалисайты. Строительные компании, образование, торговля, инфобизнес, шоу-индустрия и туризм, производство и услуги... Просто запросите у нас портфолио!",
     },
     { 
        src1 :  "img/service_4.jpg", 
        src2  : "img/service_4.jpg",
        src3  : "img/service_4.jpg",
        src4 : "img/service_4.jpg",
        src5  : "img/service_4.jpg",
        src6  : "img/service_4.jpg",
        txt1 : "Текст 4.  За 11 лет разработки сайтов клиентами компании стали более 300 российских и зарубежных компаний. Среди них – «Китгоф», «Эриант», «Вамсунг» и «Макрософт». Адаптивный дизайн, продуманная система обратной связи, впечатляющий функционали удобная админка – все, что нужно для работы!",
        txt2 :" Ваш проект будет вести отдельный менеджер, который горит работой и спит 1 час в сутки.Напишите ему в 3 ночи в Instagram, Telegram, Viber, WhatsApp, Вконтакте, и он поможети ответит на ваш вопрос. Пожалуй, нет такой отрасли бизнеса, для которой мы не делалисайты. Строительные компании, образование, торговля, инфобизнес, шоу-индустрия и туризм, производство и услуги... Просто запросите у нас портфолио!",
     },
     { 
        src1 :  "img/service_5.jpg", 
        src2  : "img/service_5.jpg",
        src3  : "img/service_5.jpg",
        src4 : "img/service_5.jpg",
        src5  : "img/service_5.jpg",
        src6  : "img/service_5.jpg",
        txt1 : "Текст 5.  За 11 лет разработки сайтов клиентами компании стали более 300 российских и зарубежных компаний. Среди них – «Китгоф», «Эриант», «Вамсунг» и «Макрософт». Адаптивный дизайн, продуманная система обратной связи, впечатляющий функционали удобная админка – все, что нужно для работы!",
        txt2 :" Ваш проект будет вести отдельный менеджер, который горит работой и спит 1 час в сутки.Напишите ему в 3 ночи в Instagram, Telegram, Viber, WhatsApp, Вконтакте, и он поможети ответит на ваш вопрос. Пожалуй, нет такой отрасли бизнеса, для которой мы не делалисайты. Строительные компании, образование, торговля, инфобизнес, шоу-индустрия и туризм, производство и услуги... Просто запросите у нас портфолио!",
     },
     { 
        src1 :  "img/service_6.jpg", 
        src2  : "img/service_6.jpg",
        src3  : "img/service_6.jpg",
        src4 : "img/service_6.jpg",
        src5  : "img/service_6.jpg",
        src6  : "img/service_6.jpg",
        txt1 : "Текст 6.  За 11 лет разработки сайтов клиентами компании стали более 300 российских и зарубежных компаний. Среди них – «Китгоф», «Эриант», «Вамсунг» и «Макрософт». Адаптивный дизайн, продуманная система обратной связи, впечатляющий функционали удобная админка – все, что нужно для работы!",
        txt2 :" Ваш проект будет вести отдельный менеджер, который горит работой и спит 1 час в сутки.Напишите ему в 3 ночи в Instagram, Telegram, Viber, WhatsApp, Вконтакте, и он поможети ответит на ваш вопрос. Пожалуй, нет такой отрасли бизнеса, для которой мы не делалисайты. Строительные компании, образование, торговля, инфобизнес, шоу-индустрия и туризм, производство и услуги... Просто запросите у нас портфолио!",
     }
]

function getPhoto(ind){
    modalTxt[0].innerHTML = arrayPhoto[ind].txt1;
    modalTxt[1].innerHTML = arrayPhoto[ind].txt2;
}
function showProps(ind) {
    const  values=Object.values(arrayPhoto[ind]);
    console.log(values)

for (var i=0; i<modalPhoto.length; i++) {
    modalPhoto[i].src=values[i];
    modalPhotoSec[i].src=values[i];
}
}

//Задать вопрос 
let btnAsk=document.getElementById('ask_question');
modalForm=document.querySelector('.modal_form2');
btnClose2=document.getElementById('modal-close2');
overlay2=document.getElementById('btn-close2');

btnAsk.addEventListener('click', function () {
    modalActive2();
})

function modalActive2(){
    modalForm.classList.add('active');
}
function Close2(){
    modalForm.classList.remove('active');
 }
btnClose2.addEventListener('click',Close2)
overlay2.addEventListener('click',Close2)

//Оставьте отзыв
let btnFeed=document.getElementById('form_feedback');
modalForm3=document.querySelector('.modal_form3');
btnClose3=document.getElementById('modal-close3');
overlay3=document.getElementById('btn-close3');

btnFeed.addEventListener('click', function () {
    modalActive3();
})

function modalActive3(){
    modalForm3.classList.add('active');
}
function Close3(){
    modalForm3.classList.remove('active');
 }
btnClose3.addEventListener('click',Close3)
overlay3.addEventListener('click',Close3)

//Заказать звонок
let btnReq=document.getElementById('request_btn');
modalForm4=document.querySelector('.modal_form4');
btnClose4=document.getElementById('modal-close4');
overlay4=document.getElementById('btn-close4');

btnReq.addEventListener('click', function () {
    modalActive4();
})

function modalActive4(){
    modalForm4.classList.add('active');
}
function Close4(){
    modalForm4.classList.remove('active');
 }
btnClose4.addEventListener('click',Close4)
overlay4.addEventListener('click',Close4)

//burger menu
let btnBurg=document.querySelector('.header-mob-menu');
burgerMenu=document.querySelector('.burger-menu');
btnClose5=document.getElementById('modal-close5');
overlay5=document.getElementById('btn-close5');

btnBurg.addEventListener('click', function () {
    modalActive5();
})

function modalActive5(){
    burgerMenu.classList.add('active');
}
function Close5(){
    burgerMenu.classList.remove('active');
 }
btnClose5.addEventListener('click',Close5)
overlay5.addEventListener('click',Close5)

// modal our_proj
let 
projectCards=document.querySelectorAll('.card_cont_prog');
projectType=document.querySelectorAll('.category_prog');

projectCards.forEach(function (item, indexCard) {
    item.addEventListener('click', function () {
        index = indexCard;
        modalTitleValue2(index);
        modalActive();
        getPhoto(index);
        showProps(index); 
})
})
function modalActive(){
    modalForProd.classList.add('active');
}
function  modalTitleValue2(ind){
        modalTitle.innerHTML=projectType[ind].innerText;
}
function Close(){
    modalForProd.classList.remove('active');
 }
btnClose.addEventListener('click',Close)
overlay.addEventListener('click',Close)