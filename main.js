$(document).ready(function() { 

    $('#form1').validate({
        rules: {
            login: {
                required: true,
                minlength: 8,
                maxlength: 16,
            },
            tel: {
                required: true,
                minlength: 11,
            },
            checkbox:{
              required: true,
            }
        },
        messages: {
            login: {
                required: "Это поле обязательно для заполнения",
                minlength: "Логин должен быть минимум 8 символов",
                maxlength: "Максимальное число символов - 16",
            },
            tel: {
                required: "Это поле обязательно для заполнения",
                minlength: "Телефон должен быть минимум 11 символов",
            },
            checkbox:{
              required: " "
            }
        },
        submitHandler: function () {
            alert('Ваша заявка отправлена')
        }
    });  


    $('#form2').validate({
      rules: {
          login: {
              required: true,
              minlength: 8,
              maxlength: 16,
          },
          tel: {
              required: true,
              minlength: 11,
          },
          checkbox:{
            required: true,
          }
      },
      messages: {
          login: {
              required: "Это поле обязательно для заполнения",
              minlength: "Логин должен быть минимум 8 символов",
              maxlength: "Максимальное число символов - 16",
          },
          tel: {
              required: "Это поле обязательно для заполнения",
              minlength: "Телефон должен быть минимум 11 символов",
          },
          checkbox:{
            required: " "
          }
      },
      submitHandler: function () {
          alert('Ваша заявка отправлена')
      }
  });

    $('#form3').validate({
      rules: {
          login: {
              required: true,
              minlength: 8,
              maxlength: 16,
          },
          tel: {
              required: true,
              minlength: 11,
          },
          checkbox:{
            required: true,
          }
      },
      messages: {
          login: {
              required: "Это поле обязательно для заполнения",
              minlength: "Логин должен быть минимум 8 символов",
              maxlength: "Максимальное число символов - 16",
          },
          tel: {
              required: "Это поле обязательно для заполнения",
              minlength: "Телефон должен быть минимум 11 символов",
          },
          checkbox:{
            required: " "
          }
      },
      submitHandler: function () {
          alert('Ваша заявка отправлена')
      }
  }); 
});

$(document).ready(function() { 
$.fn.setCursorPosition = function(pos) {
  if ($(this).get(0).setSelectionRange) {
    $(this).get(0).setSelectionRange(pos, pos);
  } else if ($(this).get(0).createTextRange) {
    var range = $(this).get(0).createTextRange();
    range.collapse(true);
    range.moveEnd('character', pos);
    range.moveStart('character', pos);
    range.select();
  }
};
$('input[name="tel"]').click(function(){
  $(this).setCursorPosition(2);
}).mask("8(999) 999-9999");
$('input[name="tel"]').mask("8(999) 999-9999");

});


// block2 slick
$(document).ready(function() { 
 
  $(".services_slide").slick({
    rows: 2,
		dots: false,
    slidesPerRow:3,
    infinite: true,
    swipe: false,
    // arrows: true,
    // prevArrow:"<img class='prev-btn' src='img/left.png'>",
    // nextArrow:"<img class='next-btn' src='img/Right.png'>",
  
  responsive: [
    {
      breakpoint: 1025,
      settings: {
        rows: 3,
        dots: false,
        slidesPerRow:2,
        swipe: false,
        
      }
    },
    {
      breakpoint: 770,
      settings: {
        rows: 3,
        dots: false,
        slidesPerRow:2,
        infinite: true,
        swipe: false
        
      }
    },
    {
      breakpoint: 480,
      settings: {
        rows:1,
        slidesPerRow:1,
        swipe: true,
        dots: true,
        arrows: true,
        prevArrow:"<img class='prev-btn' src='img/left.png'>",
        nextArrow:"<img class='next-btn' src='img/Right.png'>"
      }
    }
  ]
});
});

// block3
$(document).ready(function() { 

    $('.reviews-slider').owlCarousel({
        items:1,
        loop:true,
        mouseDrag:false,
        touchDrag:false,
        nav:true,
        dots:false,
        navText: ["<img src='img/left.png'>", "<img src='img/right.png'>"]         
          })

          // block6
    $('.our_team_slider').owlCarousel({
        items:1,
        loop:true,
        mouseDrag:false,
        touchDrag:false,
        nav:false,
        dots:true,
        navText: ["<img src='img/left.png'>", "<img src='img/right.png'>"]       
          })
});


// block3 slick
$(document).ready(function() { 
 
    $(".product_slick_slider").slick({
      rows: 1,
      slidesPerRow:4,
      arrows: true,
      prevArrow:"<img class='prev-btn' src='img/left.png'>",
      nextArrow:"<img class='next-btn' src='img/Right.png'>",
      responsive: [
        {
          breakpoint: 770,
          settings: {
            rows: 1,
            dots: true,
            slidesPerRow:2,
            infinite: true,
            swipe: true,
            arrows: true,
            prevArrow:"<img class='prev-btn' src='img/left.png'>",
            nextArrow:"<img class='next-btn' src='img/Right.png'>"
            
          }
        },
        {
          breakpoint: 480,
          settings: {
            rows:1,
            slidesPerRow:1,
            swipe: true,
            dots: true,
            arrows: true,
            prevArrow:"<img class='prev-btn' src='img/left.png'>",
            nextArrow:"<img class='next-btn' src='img/Right.png'>"
          }
        }
      ]
    });
    
    $(".filter li a").on('click', function(){
      var filter = $(this).data('filter');
      $(".product_slick_slider").slick('slickUnfilter');
      
      if(filter == 'section_1'){
        $(".product_slick_slider").slick('slickFilter','.section_1');
      }
      else if(filter == 'section_2'){
        $(".product_slick_slider").slick('slickFilter','.section_2');
      }
      else if(filter == 'section_3'){
        $(".product_slick_slider").slick('slickFilter','.section_3');
      }
      else if(filter == 'section_4'){
        $(".product_slick_slider").slick('slickFilter','.section_4');
      }
      else if(filter == 'all'){
        
        $(".product_slick_slider").slick('slickUnfilter');
      }
    })
  });
  

// block2 modal window
$(document).ready(function(){
  $('.bxslider').bxSlider({
  nextText: '',
  prevText: '',
  pagerCustom: '#bx-pager',
  mode:'vertical',
  controls: false,
  infiniteLoop: false,
  useCSS:false

});
});
// block4
$(document).ready(function() { 
 
  $(".tariffs_slider").slick({
    slidesToShow: 4,
    slidesToScroll:4,
    infinite: false,
    swipe: false,
    // arrows: true,
    // prevArrow:"<img class='prev-btn' src='img/left.png'>",
    // nextArrow:"<img class='next-btn' src='img/Right.png'>",
  
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 4,
        infinite: false,
        swipe: true,
        // arrows: true,
        // prevArrow:"<img class='prev-btn' src='img/left.png'>",
        // nextArrow:"<img class='next-btn' src='img/Right.png'>",
        
      }
    },
    {
      breakpoint: 770,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: false,
        swipe: true,
        arrows: true,
        prevArrow:"<img class='prev-btn' src='img/left.png'>",
        nextArrow:"<img class='next-btn' src='img/Right.png'>",
        
      }
    },
    {
      breakpoint: 430,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: false,
        swipe: true,
        dots:true,
        arrows: true,
        prevArrow:"<img class='prev-btn' src='img/left.png'>",
        nextArrow:"<img class='next-btn' src='img/Right.png'>",
      }
    }
  ]
});
});


// block6 slick
$(document).ready(function() { 
 
  $(".our_team_slider2").slick({
    rows: 2,
		dots: false,
    slidesPerRow:2,
    infinite: false,
    swipe: false,
    // arrows: true,
    // prevArrow:"<img class='prev-btn' src='img/left.png'>",
    // nextArrow:"<img class='next-btn' src='img/Right.png'>",
  
  responsive: [
    {
      breakpoint: 1025,
      settings: {
        rows:1,
        dots: true,
        slidesPerRow:2,
        swipe: true,
        arrows: true,
        prevArrow:"<img class='prev-btn' src='img/left.png'>",
        nextArrow:"<img class='next-btn' src='img/Right.png'>",
        
      }
    },
    {
      breakpoint: 770,
      settings: {
        rows: 1,
        dots: true,
        slidesPerRow:2,
        // infinite: true,
        swipe:true,
        arrows: true,
        prevArrow:"<img class='prev-btn' src='img/left.png'>",
        nextArrow:"<img class='next-btn' src='img/Right.png'>",
        
      }
    },
    {
      breakpoint: 480,
      settings: {
        rows:1,
        slidesPerRow:1,
        swipe: true,
        dots: true,
        arrows: true,
        prevArrow:"<img class='prev-btn' src='img/left.png'>",
        nextArrow:"<img class='next-btn' src='img/Right.png'>",
      }
    }
  ]
});
});


